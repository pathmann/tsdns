#!/usr/bin/env python

import socket, re, sys, signal

###### VARIABLES ######
ADMIN_TOKEN = '#'
ADMIN_PASS = "2j5sfd83:SA"
PORT = 50000
INI_PATH = "./dns.ini"
###### VARIABLES ######

# dnslist handles the dns entries and syncs with the equivalent file
class dnslist(object):
  # instantiate a new dnslist object on the given filename
  def __init__(self, filename):
    self._filename = filename
    self.dns = dict()
    
    reg = re.compile(r"^(.+)=(.+)$")
    fobj = open(filename, "r")
    for line in fobj:
      mo = reg.match(line)
      if mo:
        self.dns[mo.group(1)] = mo.group(2)
      
    fobj.close()
  
  # add a new dns entry
  def add(self, dns, ip):
    self.dns[dns] = ip
    self.update()
    
  # remove a dns entry from list, substantiated by the given dns
  def remove(self, dns):
    if dns in self.dns:
      del self.dns[dns]
      update()
    
  # return the ip address of the given dns, returns None, if not in list
  def ipByDNS(self, dns):
    if dns in self.dns:
      return self.dns[dns]
    else:
      return None
     
  # sync current list to the specified file
  def update(self):
    fobj = open(self._filename, "w")
    fobj.truncate(0)
    for key in self.dns:
      fobj.write(key + "=" + self.dns[key] + "\n")
    fobj.close()

# read the file given in fpath as list and returns it
def readFileAsList(fpath):
  fobj = open(fpath, "r")
  ret = fobj.readlines()
  fobj.close()
  
  return ret

# admin commands are: list, add dns ip, del dns
# basic command syntax: ADMIN_TOKEN ADMIN_PASS command param1 param2
def handleAdminCommand(data, cli, ldns):
  reg = re.compile(r"^" + ADMIN_TOKEN + " " + ADMIN_PASS + " (.+) (.*) (.*)$")
  
  #if it does not match, we got a password missmatch
  mo = reg.match(data)
  if mo:
    if mo.group(1) == "list":
      for key in ldns.dns:
        cli.send(key + "=" + ldns.dns[key] + "\n") 
    elif mo.group(1) == ("add"):
      ldns.add(mo.group(2), mo.group(3))
    elif mo.group(1) == ("del"):
      ldns.remove(mo.group(2))
    
    
# if data contains a valid dns entry from ldns, the equivalent ip is sent to cli  
def handleDNSRequest(data, cli, ldns):
  ip = ldns.ipByDNS(data)
  
  if ip:
    cli.send(ip)
    

if __name__ == "__main__":
  rinipath = INI_PATH
  wlist = None

  # get parameters, -w for whitelist, -i for different Ini-Path
  i = 1
  while i < len(sys.argv):
    if sys.argv[i] == "-w":
      wlist = readFileAsList(sys.argv[++i])
    elif sys.argv[i] == "-i":
      rinipath = sys.argv[++i]
      
    ++i
  
  # read dns entries
  dnslist = dnslist(rinipath)
  
  # start tcp server on port PORT
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  s.bind(("", PORT))
  s.listen(5)
  
  try:
    while True:
      cli, addr = s.accept()
      data = cli.recv(1024).strip()
      
      if data:        
        if data.startswith(ADMIN_TOKEN):
          if not wlist or addr[0] in wlist:        
            handleAdminCommand(data, cli, dnslist)
        else:
          handleDNSRequest(data, cli, dnslist)
          
      cli.close()
  finally:
    s.close()
  
